See docker-compose file for renovate configuration and version.

Old behavior (pre 29.17.4): An MR to update com.sofi:foo to release-2 is created

New behavior: Said MR is not created.
